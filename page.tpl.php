<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>

  <meta name="KEYWORDS" content="zamas,negrita,rock,musica,vicenza,zamasband, zamas band,padova,verona,veneto,"/>
  
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
<br/>

<div id="header"></div>
<!-- <div id="wrapper"> -->
<div id="menu">
  <?php if (isset($secondary_links)) { ?><div id="secondary"><?php print theme('links', $secondary_links) ?></div><?php } ?>
  <?php if (isset($primary_links)) { ?><div id="primary"><?php print theme('links', $primary_links) ?></div><?php } ?>
</div>

<br/>

<div id="content">

  <?php if ( $sidebar_left || $sidebar_right ) { ?>
    <div id="sidebar">
      <div id="sidebar_left">
        <?php if ($sidebar_left) { ?>
        <?php print $sidebar_left ?>
        <?php } ?>
      </div>
      
      <div id="sidebar_right">
        <?php if ($sidebar_right) { ?>
        <?php print $sidebar_right ?>
        <?php } ?>
      </div>
    </div>
    <div id="main">
  <?php } else { ?>
    <div id="main_full">
  <?php } ?>

    <div id="breadcrumb">
        <?php //print $breadcrumb ?>
    </div>


    <?php print $help ?>
    <?php print $messages ?>

    <?php if ($title) { ?>

      <h1 class="title"><?php print $title ?></h1>


    <?php } ?>
    <?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>

    <?php print $content; ?>

  </div>




</div>

<div id="footer">
  <?php print $footer_message ?>

    <div id="author" > <!-- PLEASE DON'T REMOVE THIS SECTION -->
      Darsh template by <a href="http://www.finex.org">FiNe<strong>X</strong><em>design</em></a>
      <a href="http://www.finex.org">
        <img src="<?php print base_path() . $directory ?>/img/finex_design.gif" alt="FiNeXdesign" style="border:0"/>
      </a>
    </div>
  
</div>

<?php print $closure ?>

</body>
</html>