<?php

/***********************
* Darsh Template 2007  *
***********************/

/***********************
versione: 1.2
data: 30/10/2007
***********************/


/*
  Do not include drupal's default style sheet in this theme !
*/
function phptemplate_stylesheet_import($stylesheet, $media = 'all') {
  if (strpos($stylesheet, 'misc/drupal.css') == 0) {
    return theme_stylesheet_import($stylesheet, $media);
  }
}

?>